#include "qsettingseditor.h"

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QStringList>
#include <QDebug>
#include <utility>


namespace qsettingseditor
{

enum class SubCommand {
    CREATE,
    EDIT,
    DUMP,
    VERSION
};

static QMap<SubCommand, QString> const SubCommands {
    {SubCommand::EDIT,    "edit"},
    {SubCommand::DUMP,    "dump"},
    {SubCommand::VERSION, "version"},
};

QStringList subcommands()
{
    QStringList sc;

    for(const auto& it : SubCommands.values())
        sc << it;

    return sc;
}

const std::pair<QString,QString> InvalidItem { QString(), QString() };

/**
 * @brief readItem splits item string to extract key and value
 * @param item input QString
 * @return a pair with key and value, or InvalidItem if any error occurs
 */
std::pair<QString,QString> readItem(const QString& item)
{
    QStringList tokens = item.split("=");

    if(tokens.count() != 2)
    {
        return InvalidItem;
    }

    return { tokens.first(), tokens.last() };
}

} //qsettingseditor

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);
    QCoreApplication::setApplicationName("qsettingseditor");
    QCoreApplication::setApplicationVersion("0.4.0");

    QCommandLineParser parser;
    parser.setApplicationDescription("Create, edit and manage QSettings files");
    parser.addHelpOption();
    //parser.addVersionOption();

    QString commandSyntax{"[%1]"};
    QStringList subcommands = qsettingseditor::subcommands();

    parser.addPositionalArgument(
        "command",
        "subcommand to execute",
        commandSyntax.arg(subcommands.join("|")));

    // specify settings file. If file does not exists, it will be created
    QCommandLineOption fileOption({"f", "file"}, "file to create/open", "file");
    parser.addOption(fileOption);

    // items syntax is: key=value
    QCommandLineOption itemOption({"i", "item"}, "key/value pair to add/modify", "key=value");
    parser.addOption(itemOption);

    QCommandLineOption deleteOption({"d", "delete"}, "key to delete from file", "key");
    parser.addOption(deleteOption);

    parser.process(app);

    QStringList arguments = parser.positionalArguments();

    if(arguments.count() == 0)
    {
        qCritical() << "error: missing subcommand\n";
        qInfo().noquote() << parser.helpText();
        return EXIT_FAILURE;
    }

    QString subcommand = arguments.first();

    if(!subcommands.contains(subcommand))
    {
        qCritical().noquote() << QString("error: invalid subcommand\nValid commands are: %1\n").arg(subcommands.join(","));
        qInfo().noquote() << parser.helpText();
        return EXIT_FAILURE;
    }

    if(subcommand == qsettingseditor::SubCommands[qsettingseditor::SubCommand::VERSION])
    {
        parser.showVersion();
        return EXIT_SUCCESS;
    }

    if(!parser.isSet(fileOption))
    {
        qCritical().noquote() << "error: file not specified\n";
        qInfo().noquote() << parser.helpText();
        return EXIT_FAILURE;
    }

    QSettingsEditor editor(parser.value(fileOption));

    switch(qsettingseditor::SubCommands.key(subcommand))
    {
        case qsettingseditor::SubCommand::EDIT:
            if(!parser.isSet(itemOption) && !parser.isSet(deleteOption))
            {
                qCritical().noquote() << "error: please specify --item or --delete\n";
                qInfo().noquote() << parser.helpText();
                return EXIT_FAILURE;
            }
            else
            {
                 QStringList items = parser.values(itemOption);

                 for(const auto& it : items)
                 {
                     std::pair<QString,QString> c = qsettingseditor::readItem(it);

                     if(c != qsettingseditor::InvalidItem)
                     {
                        editor.edit(c.first, c.second);
                     }
                     else
                     {
                        qWarning() << "Ignoring item" << it << "--" << "invalid syntax";
                     }
                 }

                 QStringList deleteList = parser.values(deleteOption);

                 for(const auto& it : deleteList)
                 {
                     editor.deleteKey(it);
                 }
            }
            break;
        case qsettingseditor::SubCommand::DUMP:
            editor.dump();
            break;
        default:
            break;
    }

    return EXIT_SUCCESS;

    return app.exec();
}
