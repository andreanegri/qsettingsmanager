#include <QApplication>
#include "settingsmanager.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    SettingsManager viewer;
    viewer.show();

    app.exec();

    return EXIT_SUCCESS;
}
