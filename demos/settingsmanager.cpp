#include "settingsmanager.h"
#include "ui_settingsmanager.h"
#include "qsettingsviewer.h"

SettingsManager::SettingsManager(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::SettingsManager)
{
    ui->setupUi(this);
    ui->statusbar->hide();

    QVBoxLayout* layout = dynamic_cast<QVBoxLayout*>(ui->centralwidget->layout());

    Q_ASSERT(layout);

    layout->insertWidget(0, new QSettingsViewer(this));

    connect(ui->btnClose, &QPushButton::clicked, this, &SettingsManager::close);
}

SettingsManager::~SettingsManager()
{
    delete ui;
}
