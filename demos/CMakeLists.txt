add_executable(settingsviewer
    "main.cpp"
    "settingsmanager.cpp"
)

set_property(TARGET settingsviewer PROPERTY AUTOMOC ON)
set_property(TARGET settingsviewer PROPERTY AUTOUIC ON)

target_link_libraries(settingsviewer
    PRIVATE
        qsettingsmanager
)
