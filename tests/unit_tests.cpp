#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"

#include "qsettingseditor.h"
#include <QFile>

TEST_CASE("Test QSettingsEditor")
{
    QString testfileName("testfile");
    QFile testfile(testfileName);

    if(testfile.exists())
        testfile.remove();

    // init QSettingsEditor object
    QSettingsEditor ed(testfileName);

    SECTION("add a new key")
    {
        ed.edit("key1", "val1");
        REQUIRE(ed.keysCount() == 1);
    }

    SECTION("edit existing key")
    {
        ed.edit("key1", "val1");
        REQUIRE(ed.keysCount() == 1);

        ed.edit("key1", "val2");
        REQUIRE(ed.keysCount() == 1);
    }

    SECTION("bulk edit")
    {
        ed.bulkEdit({"key1", "key2"}, {"val1", "val2"});
        REQUIRE(ed.keysCount() == 2);
    }

    SECTION("delete key")
    {
        ed.edit("key1", "val1");
        ed.edit("key2", "val2");
        ed.edit("key3", "val3");
        REQUIRE(ed.keysCount() == 3);

        ed.deleteKey("key1");
        REQUIRE(ed.keysCount() == 2);
    }

    SECTION("try to delete not existing key")
    {
        ed.edit("key1", "val1");
        ed.edit("key2", "val2");

        ed.deleteKey("key3");
        REQUIRE(ed.keysCount() == 2);
    }

    SECTION("create keys in groups")
    {
        ed.edit("group1/key1", "val1");
        ed.edit("group2/key1", "val2");

        REQUIRE(ed.keysCount() == 2);

        REQUIRE(ed.map().keys().count() == 2);
        REQUIRE(ed.map().keys().contains("group1"));
        REQUIRE(ed.map().keys().contains("group2"));
    }
}
