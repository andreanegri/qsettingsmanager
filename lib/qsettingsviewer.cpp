#include "qsettingsviewer.h"
#include "qsettingsentry.h"
#include "ui_qsettingsviewer.h"
#include "qsettingsinputdialog.h"

#include <QFileDialog>
#include <QDebug>
#include <QVBoxLayout>
#include <QLabel>
#include <QGroupBox>


QSettingsViewer::QSettingsViewer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QSettingsViewer)
{
    ui->setupUi(this);

    ui->leSelect->setReadOnly(true);
    ui->btnAdd->setEnabled(false);
    ui->btnSave->setEnabled(false);

    connections();
}

QSettingsViewer::~QSettingsViewer()
{
    delete ui;
}

void QSettingsViewer::connections()
{
    connect(ui->btnSelect, &QPushButton::clicked, this, &QSettingsViewer::onSelectSettingsFileClicked);
    connect(ui->btnSave,   &QPushButton::clicked, this, &QSettingsViewer::onSaveClicked);
    connect(ui->btnAdd,    &QPushButton::clicked, this, &QSettingsViewer::onAddClicked);
}

void QSettingsViewer::fillSettings()
{
    _settingsEditor = std::unique_ptr<QSettingsEditor>(new QSettingsEditor(ui->leSelect->text()));
    QSettingsEditor::SettingsMap mainMap = _settingsEditor->map();

    QVBoxLayout* layout = settingsLayout();

    Q_ASSERT(layout);

    for(const auto& groupName : mainMap.keys())
    {
        QGroupBox* gb = new QGroupBox(groupName);

        QVBoxLayout* vLayout = new QVBoxLayout(gb);
        vLayout->setObjectName(groupName + "_layout");

        layout->addWidget(gb);

        for(const auto& groupKey : mainMap[groupName].keys())
        {
            QSettingsEntry* e = new QSettingsEntry(groupKey, mainMap[groupName].value(groupKey).toString(), gb);
            connect(e, &QSettingsEntry::deleteRequested, this, &QSettingsViewer::onDeleteRequested);

            gb->layout()->addWidget(e);
        }
    }
}

void QSettingsViewer::clearGui()
{
    QList<QSettingsEntry*> entries = ui->scrollAreaWidgetContents->findChildren<QSettingsEntry*>();

    for(const auto& entry : entries)
    {
        delete entry;
    }

    QList<QGroupBox*> groupBoxes = ui->scrollAreaWidgetContents->findChildren<QGroupBox*>();

    for(const auto& gb : groupBoxes)
    {
        delete gb;
    }
}

QVBoxLayout *QSettingsViewer::settingsLayout() const
{
    return dynamic_cast<QVBoxLayout*>(ui->scrollAreaWidgetContents->layout());
}

QString QSettingsViewer::getGroup(QSettingsEntry *entry)
{
    QGroupBox* parentGb = dynamic_cast<QGroupBox*>(entry->parent());

    Q_ASSERT(parentGb);

    return parentGb->title();
}

void QSettingsViewer::onSelectSettingsFileClicked()
{
    QString selected = QFileDialog::getOpenFileName(this, "Select QSettings file");

    if(selected.isEmpty())
    {
        return;
    }
    else
    {
        ui->leSelect->setText(selected);
        ui->btnSave->setEnabled(true);
        ui->btnAdd->setEnabled(true);

        clearGui();

        fillSettings();
    }
}

void QSettingsViewer::onSaveClicked()
{
    QStringList keys;
    QVariantList values;

    for(const auto& gb : ui->scrollAreaWidgetContents->findChildren<QGroupBox*>())
    {
        for(const auto& e : gb->findChildren<QSettingsEntry*>())
        {
            if(gb->title().isEmpty())
            {
                keys   << e->key();
                values << e->value();
            }
            else
            {
                keys   << gb->title() + "/" + e->key();
                values << e->value();
            }
        }
    }

    _settingsEditor->bulkEdit(keys, values);
}

void QSettingsViewer::onAddClicked()
{
    QSettingsInputDialog dlg(this);
    dlg.setWindowTitle("Insert new settings");

    dlg.exec();

    if(dlg.result() == QDialog::Accepted)
    {
        QString key;
        if(dlg.group().isEmpty())
            key = dlg.key();
        else
            key = dlg.group() + "/" + dlg.key();

        _settingsEditor->edit(key, dlg.value());

        clearGui();

        fillSettings();
    }
}

void QSettingsViewer::onDeleteRequested(const QString &key)
{
    QSettingsEntry* e = dynamic_cast<QSettingsEntry*>(sender());

    Q_ASSERT(e);

    _settingsEditor->deleteKey(key, getGroup(e));

    clearGui();

    fillSettings();
}
