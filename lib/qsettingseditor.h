#ifndef QSETTINGSEDITOR_H
#define QSETTINGSEDITOR_H

#include <QSettings>

#include <memory>

class QSettingsEditor
{
public:
    typedef QMap<QString, QSettings::SettingsMap> SettingsMap;

    explicit QSettingsEditor(const QString& filename,
                             QSettings::Format format = QSettings::IniFormat);
    virtual ~QSettingsEditor() {}

    /**
     * @brief dump prints on terminal currently saved settings
     */
    void dump() const;

    /**
     * @brief save stores on permanent storage current settings
     */
    void save();

    /**
     * @brief edit changes passed setting key with desired value
     * @param key setting key to be changed
     * @param value new value for requested key
     * @param forceSync if set, force a settings save on permanent storage
     */
    void edit(const QString& key, const QVariant& value, bool forceSync = true);

    /**
     * @brief bulkEdit edit a list of keys with new values. Note that sync is
     * performed at the end
     */
    void bulkEdit(const QStringList& keys, const QVariantList& values);

    /**
     * @brief map return a map representing current keys-values pairs
     */
    SettingsMap map() const;

    /**
     * @brief deleteKey
     * @param key
     */
    void deleteKey(const QString& key, const QString& group = QString());

    /**
     * @brief keysCount
     * @return
     */
    int keysCount();

private:
    std::unique_ptr<QSettings> _settings;
};

#endif // QSETTINGSEDITOR_H
