#ifndef QSETTINGSINPUTDIALOG_H
#define QSETTINGSINPUTDIALOG_H

#include <QDialog>

namespace Ui {
class QSettingsInputDialog;
}

class QSettingsInputDialog : public QDialog
{
    Q_OBJECT

public:
    explicit QSettingsInputDialog(QWidget *parent = nullptr);
    ~QSettingsInputDialog();

    QString key() const;
    QString value() const;
    QString group() const;

    void setKey(const QString& key);
    void setValue(const QString& value);
    void setGroup(const QString& group);

    bool okButtonEnabled();

protected slots:
    void checkInsertedText();


private:
    Ui::QSettingsInputDialog *ui;
};

#endif // QSETTINGSINPUTDIALOG_H
