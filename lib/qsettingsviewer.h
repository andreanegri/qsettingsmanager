#ifndef QSETTINGSVIEWER_H
#define QSETTINGSVIEWER_H

#include "qsettingseditor.h"

#include <QWidget>
#include <memory>

class QVBoxLayout;
class QSettingsEntry;

namespace Ui {
class QSettingsViewer;
}

class QSettingsViewer : public QWidget
{
    Q_OBJECT

public:
    explicit QSettingsViewer(QWidget *parent = nullptr);
    ~QSettingsViewer();

private:
    void connections();
    void fillSettings();
    void clearGui();

    QVBoxLayout* settingsLayout() const;

    QString getGroup(QSettingsEntry*);

protected slots:
    void onSelectSettingsFileClicked();
    void onSaveClicked();
    void onAddClicked();
    void onDeleteRequested(const QString& key);

private:
    Ui::QSettingsViewer *ui;
private:
    std::unique_ptr<QSettingsEditor> _settingsEditor;
};

#endif // QSETTINGSVIEWER_H
