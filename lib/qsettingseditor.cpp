#include "qsettingseditor.h"

#include <QDebug>


/**
 * @brief QSettingsEditor::QSettingsEditor
 * @param filename settings file to edit
 * @param format settings file format
 */
QSettingsEditor::QSettingsEditor(const QString &filename, QSettings::Format format)
{
    _settings = std::unique_ptr<QSettings>(new QSettings(filename, format));
}

void QSettingsEditor::dump() const
{
    for(const auto& k : _settings->allKeys())
    {
        qDebug() << k << _settings->value(k).toString();
    }
}

void QSettingsEditor::save()
{
    _settings->sync();
}

void QSettingsEditor::edit(const QString &key, const QVariant &value, bool forceSync)
{
    _settings->setValue(key, value);

    if(forceSync)
    {
        this->save();
    }
}

void QSettingsEditor::bulkEdit(const QStringList &keys, const QVariantList &values)
{
    for(int i=0; i<keys.count(); i++)
    {
        this->edit(keys[i], values[i], false);
    }

    this->save();
}

QSettingsEditor::SettingsMap QSettingsEditor::map() const
{
    QSettingsEditor::SettingsMap resultMap;

    QSettings::SettingsMap map;

    // get root keys
    for(const auto& k : _settings->childKeys())
    {
        map[k] =  _settings->value(k);
    }

    if(map.count())
        resultMap[""] = map;

    for(const auto& group : _settings->childGroups())
    {
        _settings->beginGroup(group);

        QSettings::SettingsMap map;

        // get root keys
        for(const auto& k : _settings->childKeys())
        {
            map[k] =  _settings->value(k);
        }

        resultMap[group] = map;

        _settings->endGroup();
    }

    return resultMap;
}

void QSettingsEditor::deleteKey(const QString &key, const QString& group)
{
    if(group.isEmpty())
    {
        _settings->remove(key);
    }
    else
    {
        _settings->beginGroup(group);
        _settings->remove(key);
        _settings->endGroup();
    }

    this->save();
}

int QSettingsEditor::keysCount()
{
    return _settings->allKeys().count();
}
