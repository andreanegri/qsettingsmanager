#include "qsettingsinputdialog.h"
#include "ui_qsettingsinputdialog.h"

QSettingsInputDialog::QSettingsInputDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::QSettingsInputDialog)
{
    ui->setupUi(this);

    connect(ui->btnOk,     &QPushButton::clicked,   this, &QSettingsInputDialog::accept);
    connect(ui->btnCancel, &QPushButton::clicked,   this, &QSettingsInputDialog::reject);
    connect(ui->leKey,     &QLineEdit::textChanged, this, &QSettingsInputDialog::checkInsertedText);
    connect(ui->leGroup,   &QLineEdit::textChanged, this, &QSettingsInputDialog::checkInsertedText);
}

QSettingsInputDialog::~QSettingsInputDialog()
{
    delete ui;
}

QString QSettingsInputDialog::key() const
{
    return ui->leKey->text();
}

QString QSettingsInputDialog::value() const
{
    return ui->leValue->text();
}

QString QSettingsInputDialog::group() const
{
    return ui->leGroup->text();
}

void QSettingsInputDialog::setKey(const QString &key)
{
    ui->leKey->setText(key);
}

void QSettingsInputDialog::setValue(const QString &value)
{
    ui->leValue->setText(value);
}

void QSettingsInputDialog::setGroup(const QString &group)
{
    ui->leGroup->setText(group);
}

bool QSettingsInputDialog::okButtonEnabled()
{
    return ui->btnOk->isEnabled();
}

void QSettingsInputDialog::checkInsertedText()
{
    ui->btnOk->setEnabled(
        !ui->leKey->text().contains("/") &&
        !ui->leGroup->text().contains("/")
    );
}
