#include "qsettingsentry.h"
#include "ui_qsettingsentry.h"

QSettingsEntry::QSettingsEntry(const QString& key, const QString& value, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QSettingsEntry)
{
    ui->setupUi(this);

    ui->key->setText(key);
    ui->key->setToolTip(key);
    ui->value->setText(value);

    connect(ui->btnDelete, &QPushButton::clicked, this, &QSettingsEntry::onDeleteClicked);
}

QSettingsEntry::~QSettingsEntry()
{
    delete ui;
}

QString QSettingsEntry::key() const
{
    return ui->key->text();
}

QString QSettingsEntry::value() const
{
    return ui->value->text();
}

void QSettingsEntry::onDeleteClicked()
{
    emit deleteRequested(ui->key->text());
}
