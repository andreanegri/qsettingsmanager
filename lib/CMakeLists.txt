add_library(qsettingsmanager SHARED
    "qsettingseditor.cpp"
    "qsettingsviewer.cpp"
    "qsettingsentry.cpp"
    "qsettingsinputdialog.cpp"
)

set_property(TARGET qsettingsmanager PROPERTY AUTOMOC ON)
set_property(TARGET qsettingsmanager PROPERTY AUTOUIC ON)

target_link_libraries(qsettingsmanager
    PUBLIC
        Qt5::Core
        Qt5::Widgets
)

target_include_directories(qsettingsmanager
    INTERFACE
       Qt5::Core
       $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
)
