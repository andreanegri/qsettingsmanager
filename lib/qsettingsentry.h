#ifndef QSETTINGSENTRY_H
#define QSETTINGSENTRY_H

#include <QWidget>

namespace Ui {
class QSettingsEntry;
}

class QSettingsEntry : public QWidget
{
    Q_OBJECT

public:
    explicit QSettingsEntry(const QString& key, const QString& value, QWidget *parent = nullptr);
    ~QSettingsEntry();

    QString key() const;
    QString value() const;

protected slots:
    void onDeleteClicked();

signals:
    void deleteRequested(const QString& key);

private:
    Ui::QSettingsEntry *ui;
};

#endif // QSETTINGSENTRY_H
